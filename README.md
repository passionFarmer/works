- [作品集](#%08%E4%BD%9C%E5%93%81%E9%9B%86)
    - [EDM](#edm)
    - [rabbitMQ](#rabbitmq)
    - [露天後台](#%E9%9C%B2%E5%A4%A9%E5%BE%8C%E5%8F%B0)
    - [物流(CVS)](#%E7%89%A9%E6%B5%81cvs)
    - [露露通 api](#%E9%9C%B2%E9%9C%B2%E9%80%9A-api)
    - [Bug處理(Mantis)](#bug%E8%99%95%E7%90%86mantis)
    - [前台各功能頁](#%08%E5%89%8D%E5%8F%B0%E5%90%84%08%E5%8A%9F%E8%83%BD%E9%A0%81)
    - [Code review](#code-review)

# 作品集

## EDM

- 權限管理
- cron
- rsync

![cron](pic/edm_sent_group.png)
![list](pic/edm_list.png)
![statistic](pic/edm_statistic.png)

## rabbitMQ

- 大量處理取消交易/費用

![process](pic/new_cancel_chargefee.png)
![mq1](pic/mq_1.png)
![mq2](pic/mq_2.png)
![mq3](pic/mq_3.png)
![mq4](pic/mq_4.png)

## 露天後台

- 會員、金流、`物流`、商品、廣告

![bidadm](pic/bidadm_1.png)

## 物流(CVS)

- 貨態追蹤
- 通知訊息監控
- ftp 交換資料

![cvs](pic/bidadm_cvs_1.png)
![cvs](pic/bidadm_cvs_2.png)
![cvs](pic/bidadm_cvs_3.png)

## 露露通 api

- 會員資訊
- 黑名單資訊
- 商品資訊

![ruru](pic/ruten_ruru.png)

## Bug處理(Mantis)

- 各部門回報平台
- 協調 bug 處理部門及工程師

![bug](pic/mantis_1.png)
![bug](pic/mantis_5.png)
![bug](pic/mantis_6.png)
![bug](pic/trello_1.png)

## 前台各功能頁

- ajax 呼叫頁
- 列表頁按鈕/狀態顯示

![ruten](pic/ruten_acc.png)
![ruten](pic/ruten_lis.png)

## Code review

![review](pic/go_4.png)
![review](pic/go_3.png)